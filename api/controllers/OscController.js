/**
 * OpcController
 *
 * @description :: Server-side logic for managing opcs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	setMode: function(req, res){
	  var mode = req.param('mode');
	  sails.services.osc.setMode(mode, function(data){
	    res.send("current mode: " + data);
	  })
	},
  getParams: function(req, res){
    res.json(req.session.glados);
  },
	action: function(req, res){
	  var action = req.param('action');
	  var param = req.param('param');

    sails.services.osc.send(action, param);
    res.send(action + param);
	}
};

