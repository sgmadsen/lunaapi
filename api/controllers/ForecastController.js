/**
 * ForecastController
 *
 * @description :: Server-side logic for managing Forecasts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
// Require the module
var Forecast = require('forecast');

// Initialize
var forecast = new Forecast({
  service: 'forecast.io',
  key: 'a1846fe1e05b6e3c354577fd8ee89b5c',
  units: 'fahrenheit', // Only the first letter is parsed
  cache: true,      // Cache API requests?
  ttl: {            // How long to cache requests. Uses syntax from moment.js: http://momentjs.com/docs/#/durations/creating/
    minutes: 27,
    seconds: 45
    }
});

module.exports = {
	getHome: function(req, res){
	  // Retrieve weather information from coordinates (Sydney, Australia)
    forecast.get([40.770055, -111.855443], function(err, weather) {
      if(err) return console.dir(err);
      console.dir(weather);
      res.send(weather);
    });
	}
};

