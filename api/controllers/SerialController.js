/**
 * SerialController
 *
 * @description :: Server-side logic for managing Serials
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
 var serialjs=require('serialport-js');
 var host = process.env.DEVICE || '/dev/ttyACM0';

module.exports = {
	init: function(req, res){

   serialjs.open(
       host,
       start,
       '\n'
   );
   function start(port){
       port.on(
           'data',
           gotData
       );
   }
    function gotData(data){
        console.log(data);
    }

	},
	find: function(){
    serialjs.find(
        function(ports){
            console.log('available usb serial : ',ports);
        }
    );
	},
	send: function(req, res){
	   serialjs.open(
         host,
         send,
         '\n'
     );
     function send(port){
         port.send(req.param('message'));
         res.json(port);
     }
	}
};

