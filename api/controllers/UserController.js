/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	getMe: function(req, res) {
		if(req.user){
			res.json(req.user);
		} else {
			res.json({error: "not authed"});
		}
	},
	createAdmin: function(user){
	  User.findOrCreate(user).exec(function(err, data){
	    console.log(user);
	  })
	}
};

