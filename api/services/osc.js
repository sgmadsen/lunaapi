var osc = require('node-osc');
var oscServer = new osc.Client('127.0.0.1', 5001);

var OscController = {
  connect: function(cb){
    oscServer.send('/luna', "mode", "2", function() {
      cb();
    });
  },
  setMode: function(mode){
    OscController.send("mode", mode);
  },
  send: function(action, param1){
    oscServer.send('/luna', action, param1);
  },
  sendArray: function(action, params){
    oscServer.send('/luna', action, params[0], params[1], params[2]);
  }
}
module.exports = OscController;
